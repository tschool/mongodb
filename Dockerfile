FROM mongo:latest
COPY ./init.js /docker-entrypoint-initdb.d/
EXPOSE 27017:27017
EXPOSE 443:443
EXPOSE 4200:4200
EXPOSE 4567:4567

